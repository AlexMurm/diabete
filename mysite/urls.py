﻿from django.conf.urls import patterns, include, url
from django.contrib import admin
from mysite import settings
from django.views.generic import RedirectView

urlpatterns = patterns('',
    #url(r'^polls/', include('polls.urls', namespace="polls")),
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('diabete.urls', namespace="diabete")),
)

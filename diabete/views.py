﻿from django.http import HttpResponse
from django.views import generic
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from diabete.forms import UserCreateForm
from django.contrib.auth import authenticate, login
import datetime
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext
from diabete.models import Product, Note_diary_day, Note_diary, Note_diary_product
from diabete.my_func import my_trans
from django import forms


class IndexView(generic.ListView):
    #model = Product
    template_name = 'diabete/index.html'
    context_object_name = 'products_for_select'

    def get_queryset(self):
        return Product.objects.order_by('product_name')

    '''@method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(IndexView, self).dispatch(*args, **kwargs)'''

class Google(generic.TemplateView):
    template_name = 'diabete/google8337acc13a0675a4.html'

class Yandex(generic.TemplateView):
    template_name = 'diabete/0c78070968be.html'

def about(request):
    return render(request,'diabete/about.html')

def contacts(request):
    return render(request,'diabete/contacts.html')

def video(request):
    return render(request,'diabete/video.html')

def delete_fav(request):
    for product in request.POST:
        product_delete = request.user.favorite_product_set.filter(product_name=product)
        product_delete.delete()
    return HttpResponseRedirect(reverse('diabete:fav_products'))

def adding_to_fav(request):
    products = [] #name, GI, carb; -3: csrf, product (selected), coef
    ids=[i[len('product_name'):] for i in request.POST if i.startswith('product_name')] #id of products
    ids.sort(key=(lambda x: int(x)))
    for i in range(len(ids)):
        products.append([])
        for j in range(3):
            products[i].append('')
        products[i].append(ids[i])
    for i in request.POST:
        if i.startswith('product_name'):
            products[ids.index(i[len('product_name'):])][0]=request.POST[i].replace('\t','').replace('\n','').strip()
        if i.startswith('product_GI'):
            products[ids.index(i[len('product_GI'):])][1]=request.POST[i].replace(',','.')
        if i.startswith('product_carb'):
            products[ids.index(i[len('product_carb'):])][2]=request.POST[i].replace(',','.')
    if request.user.is_authenticated():
        for product_id in ids:
            name_of_product = products[ids.index(product_id)][0] if products[ids.index(product_id)][0] else "Имя не задано"
            if (request.user.favorite_product_set.filter(product_name=name_of_product)):
                product = request.user.favorite_product_set.filter(product_name=name_of_product)
                product.delete()

            new_product = request.user.favorite_product_set.create(product_name=name_of_product,
product_GI=products[ids.index(product_id)][1],product_carb=products[ids.index(product_id)][2],
pub_date=datetime.datetime.now())

    return HttpResponseRedirect(reverse('diabete:fav_products'))

def result(request):
    error_message_cant_calculate = ''
    error_message_diary = ''
    _total_carb=0
    products = [] #name, GI, carb, gram; -3: csrf, product (selected), coef
    ids=[i[len('product_name'):] for i in request.POST if i.startswith('product_name')] #id of products
    ids.sort(key=(lambda x: int(x)))
    for i in range(len(ids)):
        products.append([])
        for j in range(4):
            products[i].append('')
        products[i].append(ids[i])
    for i in request.POST:
        if i.startswith('product_name'):
            products[ids.index(i[len('product_name'):])][0]=request.POST[i].replace('\t','').replace('\n','').strip()
        if i.startswith('product_GI'):
            products[ids.index(i[len('product_GI'):])][1]=request.POST[i].lstrip('-').replace(',','.')
        if i.startswith('product_carb'):
            products[ids.index(i[len('product_carb'):])][2]=request.POST[i].lstrip('-').replace(',','.')
        if i.startswith('product_mass'):
            products[ids.index(i[len('product_mass'):])][3]=request.POST[i].lstrip('-').replace(',','.')
    for i in range(len(products)):
        try:
            _total_carb += float(products[i][3])*float(products[i][2])/100
        except:
            pass
    try:
        coef = request.POST['coef'].replace(',','.')
    except:
        coef = '0'
    try:
        _dose = float(coef)*_total_carb/12
    except:
        _dose = 0
        error_message_cant_calculate = 'Ошибка обработки данных. Убедитесь в корректности вводимых значений.'
    _dose = '%.2f' % _dose
    _total_carb = '%.2f' % _total_carb
    time_now = datetime.datetime.now()
    if ids:
        products_max_index = max(int(ids[-1]),Product.objects.latest('id').id)
    else:
        products_max_index = Product.objects.latest('id').id
    years = list(range(2013,time_now.year+1))
    months = ['январь','февраль','март','апрель','май','июнь','июль','август','сентябрь','октябрь','ноябрь','декабрь']
    days = list(range(1,32))
    hours = list(range(0,24))
    minutes = list(range(0,60))
    seconds = list(range(0,60))
    if 'edit_note_diary' in request.POST:
        edit_note_diary = request.POST['edit_note_diary']
        note_diary_day_id = request.POST['note_diary_day_id']
        note_diary_id = request.POST['note_diary_id']
    else:
        edit_note_diary = ''
        note_diary_day_id = ''
        note_diary_id = ''
    if 'glu_level' in request.POST:
        glu_level = request.POST['glu_level'].replace(',','.')
        dose_by_user = request.POST['dose'].replace(',','.')
        try:
            time=datetime.datetime.strptime(request.POST['year']+'.'+request.POST['month']+'.'+request.POST['day']+'.'+request.POST['hour']+'.'+request.POST['minute']+'.'+request.POST['second'],"%Y.%m.%d.%H.%M.%S")
        except:
            if request.POST['month'] == '2':
                date_day = '28'
            else:
                date_day = '30'
            time = datetime.datetime.strptime(request.POST['year']+'.'+request.POST['month']+'.'+date_day+'.'+request.POST['hour']+'.'+request.POST['minute']+'.'+request.POST['second'],"%Y.%m.%d.%H.%M.%S")
            error_message_diary = 'Указанной Вами даты не существует. Пожалуйста, произведите расчет занаво.'
    else:
        glu_level = '0.0'
        dose_by_user = '0.0'
        time = time_now
    if 'calc' in request.POST or request.method=='GET':
        pass
    else: # записать в дневник
        utc_diff=datetime.datetime.now()-datetime.datetime.utcnow()
        format_day = '%Y.%m.%d'
        format_note_diary = '%H:%M:%S'
        if error_message_diary:
            pass
        else:
            if time>time_now:
                error_message_diary = 'Запись в дневник не может быть осуществлена на будущую дату. Пожалуйста, произведите расчет занаво.'
    if error_message_diary or 'calc' in request.POST or request.method=='GET':
        return render(request, 'diabete/result.html',{
                'products_for_select': Product.objects.order_by('product_name'),
                'coef':coef,
                'products':products,
                'dose': _dose,
                'dose_by_user':dose_by_user,
                'total_carb': _total_carb,
                'glu_level': glu_level,
                'products_max_index': products_max_index,
                'error_message_cant_calculate': error_message_cant_calculate,
                'error_message_diary':error_message_diary,
                'time':time,
                'years':years,
                'months':months,
                'days':days,
                'hours':hours,
                'minutes':minutes,
                'seconds':seconds,
                'edit_note_diary':edit_note_diary,
                'note_diary_day_id':note_diary_day_id,
                'note_diary_id':note_diary_id,
            })
    else:
        if ((request.user.note_diary_day_set.count()==0 or (not request.user.note_diary_day_set.filter(name=time.strftime(format_day))))): #request.user.note_diary_day_set.last().pub_date+utc_diff
            current_note_diary_day = request.user.note_diary_day_set.create(name=time.strftime(format_day),pub_date=time)
        else:
            current_note_diary_day = request.user.note_diary_day_set.filter(name=time.strftime(format_day))[0]

        current_note_diary = current_note_diary_day.note_diary_set.create(name=time.strftime(format_note_diary),dose=request.POST['dose'].replace(',','.'),total_carb=_total_carb,pub_date=time,glu_level=request.POST['glu_level'].replace(',','.'))
        for product_id in ids:
            try:
                note_product = current_note_diary.note_diary_product_set.create(product_name=products[ids.index(product_id)][0],
product_GI=products[ids.index(product_id)][1],product_carb=products[ids.index(product_id)][2],product_mass=products[ids.index(product_id)][3],
pub_date=time)
            except:
                pass
        if note_diary_id:
            try:
                request.user.note_diary_day_set.get(id=note_diary_day_id).note_diary_set.get(id=note_diary_id).delete()
            except:
                pass
        return HttpResponseRedirect("%s?date=%i_%i_%i" % (reverse('diabete:diary'),time.year,time.month-1,time.day))

def fav_products(request):
    if not request.user.is_authenticated():
        return  HttpResponseRedirect(reverse('diabete:index'))
    products_for_select = Product.objects.order_by('product_name')
    users_fav_products = request.user.favorite_product_set.order_by('product_name')
    return render(request, 'diabete/fav_products.html',{
        'products_for_select':products_for_select,
        'users_fav_products':users_fav_products,
    })

def diary(request):
    if not request.user.is_authenticated():
        return  HttpResponseRedirect(reverse('diabete:index'))
    format_date = '%Y.%m.%d'
    format_string = '%A, %d %B %Y'
    format_weekday = '%A'
    format_month = '%B'
    if 'date' not in request.GET:
        date = datetime.datetime.now()
        get_date = [date.year,date.month-1,date.day]
    elif request.GET['date'] == 'today':
        date = datetime.datetime.now()
        get_date = [date.year,date.month-1,date.day]
    else:
        get_date = request.GET['date'].split('_') #[year,month-1,day]
        date=datetime.datetime.strptime("%s,%i,%s" % (get_date[0],int(get_date[1])+1,get_date[2]),"%Y,%m,%d")
    note_date = date.strftime(format_string)
    note_weekday = date.strftime(format_weekday)
    note_month = date.strftime(format_month)
    current_note_diary_day_list = request.user.note_diary_day_set.filter(name=date.strftime(format_date))
    if current_note_diary_day_list:
        current_note_diary_day = current_note_diary_day_list[0]
    else:
        current_note_diary_day = None
    return render(request, 'diabete/diary.html',{
            'note_date':my_trans(note_date,note_weekday,note_month),
            'year': get_date[0],
            'month':get_date[1],
            'day':get_date[2],
            'current_note_diary_day':current_note_diary_day
    })

def edit_note(request):
    if request.method == 'GET':
        return HttpResponseRedirect(reverse('diabete:index'))
    note_diary_day_id = request.POST['note_diary_day_id']
    date = request.user.note_diary_day_set.get(id=note_diary_day_id)
    date_next = date.name.split('.')
    if 'btn_delete_note' in request.POST:
        date.note_diary_set.get(id=request.POST['note_id']).delete()
        return HttpResponseRedirect("%s?date=%s_%i_%s" % (reverse('diabete:diary'),date_next[0],int(date_next[1])-1,date_next[2]))
    else:
        note_diary_id = request.POST['note_id']
        note = date.note_diary_set.get(id=note_diary_id)
        time_now = datetime.datetime.now()
        years = list(range(2013,time_now.year+1))
        months = ['январь','февраль','март','апрель','май','июнь','июль','август','сентябрь','октябрь','ноябрь','декабрь']
        days = list(range(1,32))
        hours = list(range(0,24))
        minutes = list(range(0,60))
        seconds = list(range(0,60))
        error_message_cant_calculate = ''
        error_message_diary = ''
        _total_carb = note.total_carb
        products = []
        products_max_index = Product.objects.latest('id').id
        for product in note.note_diary_product_set.all():
            products.append([])
            i=len(products)-1
            products[i].append(product.product_name)
            products[i].append(product.product_GI)
            products[i].append(product.product_carb)
            products[i].append(product.product_mass)
            products[i].append(products_max_index+1)
            products_max_index += 1
        if 'btn_edit_note' in request.POST:
            edit_note_diary = 'yes'
            time = datetime.datetime.strptime(date.name+'.'+note.name,'%Y.%m.%d.%H:%M:%S')
            _dose = note.dose
            try:
                dose_by_user = round(float(_dose)*2,0)/2
                dose_by_user = '%.1f' % dose_by_user
            except:
                dose_by_user = '0.0'
            glu_level = note.glu_level
            try:
                coef = float(_dose)/float(_total_carb)*12
                coef = '%.2f' % coef
            except:
                coef = '0'
        else: #btn_template - использовать запись как шаблон
            edit_note_diary = ''
            time = time_now
            dose_by_user = '0.0'
            _dose = '0.00'
            glu_level = '0.0'
            coef = '0'
        return render(request, 'diabete/result.html',{
                'products_for_select': Product.objects.order_by('product_name'),
                'coef':coef,
                'products':products,
                'dose': _dose,
                'dose_by_user':dose_by_user,
                'total_carb': _total_carb,
                'glu_level':glu_level,
                'products_max_index': products_max_index,
                'error_message_cant_calculate': error_message_cant_calculate,
                'error_message_diary':error_message_diary,
                'time':time,
                'years':years,
                'months':months,
                'days':days,
                'hours':hours,
                'minutes':minutes,
                'seconds':seconds,
                'edit_note_diary':edit_note_diary,
                'note_diary_day_id':note_diary_day_id,
                'note_diary_id':note_diary_id,
            })

def register(request):
    #context = RequestContext(request)
    if request.method == "POST":
        user_form = UserCreateForm(request.POST)
        if user_form.is_valid():
            username = user_form.clean_username()
            password = user_form.clean_password2()
            user_form.save()
            user = authenticate(username=username,password=password)
            login(request, user)
            return redirect('diabete:index')
    else:
        user_form = UserCreateForm()
    return render(request,'diabete/register.html',
                  { 'form' : user_form, 'title':'Регистрация' })
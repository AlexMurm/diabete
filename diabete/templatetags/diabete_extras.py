﻿from django import template
import datetime
#from django.template.defaulttags import register as _register

register = template.Library()

def current_time(args):
    format_string, delta = args.split(';')
    return (datetime.datetime.utcnow()+datetime.timedelta(minutes=float(delta))).strftime(format_string)

register.simple_tag(current_time)

#@_register.filter
def get_dict(list_dict,args): #получить словарь по значению одного ключа (.filter(key=val))
    key, val = args.split(';')
    for d in list_dict:
        if d.get(key)==name:
            return d
    return None

register.filter('get_dict', get_dict)
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('diabete', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Favorite_product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_name', models.CharField(max_length=100)),
                ('product_GI', models.CharField(max_length=4)),
                ('product_carb', models.CharField(max_length=4)),
                ('pub_date', models.DateTimeField(verbose_name='date published')),
                ('username', models.ForeignKey(default='', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_name',
            field=models.CharField(max_length=100),
        ),
    ]

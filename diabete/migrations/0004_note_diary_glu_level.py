# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('diabete', '0003_auto_20141125_0020'),
    ]

    operations = [
        migrations.AddField(
            model_name='note_diary',
            name='glu_level',
            field=models.CharField(default='', max_length=4),
            preserve_default=False,
        ),
    ]

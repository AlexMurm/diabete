# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('diabete', '0002_auto_20141117_1214'),
    ]

    operations = [
        migrations.CreateModel(
            name='Note_diary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=10)),
                ('dose', models.CharField(max_length=4)),
                ('total_carb', models.CharField(max_length=4)),
                ('pub_date', models.DateTimeField(verbose_name='date published')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Note_diary_day',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=10)),
                ('pub_date', models.DateTimeField(verbose_name='date published')),
                ('username', models.ForeignKey(default='', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Note_diary_product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_name', models.CharField(max_length=100)),
                ('product_GI', models.CharField(max_length=4)),
                ('product_carb', models.CharField(max_length=4)),
                ('product_mass', models.CharField(max_length=7)),
                ('pub_date', models.DateTimeField(verbose_name='date published')),
                ('note_diary', models.ForeignKey(default='', to='diabete.Note_diary')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='note_diary',
            name='note_diary_date',
            field=models.ForeignKey(default='', to='diabete.Note_diary_day'),
            preserve_default=True,
        ),
    ]

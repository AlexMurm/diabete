# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('diabete', '0005_auto_20141127_1338'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note_diary',
            name='total_carb',
            field=models.CharField(max_length=50),
            preserve_default=True,
        ),
    ]

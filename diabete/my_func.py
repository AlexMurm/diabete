﻿trans_dict = {
'Monday':'Понедельник',
'Tuesday':'Вторник',
'Wednesday':'Среда',
'Thursday':'Четверг',
'Friday':'Пятница',
'Saturday':'Суббота',
'Sunday':'Воскресенье',
'January':'января','February':'февраля','March':'марта','April':'апреля','May':'мая','June':'июня','July':'июля','August':'августа','September':'сентября','October':'октября','November':'ноября','December':'декабря',
}

def my_trans(*args):
    string=args[0]
    args=args[1:]
    for arg in args:
        string = string.replace(arg,trans_dict[arg])
    return string
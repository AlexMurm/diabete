﻿from django.db import models
from django.contrib.auth.models import User


class Product(models.Model):
    product_name = models.CharField(max_length=100)
    product_GI = models.CharField(max_length=4)
    product_carb = models.CharField(max_length=4)
    pub_date = models.DateTimeField('date published')

    def __str__(self):              # __unicode__ on Python 2
        return self.product_name

class Favorite_product(models.Model):
    username = models.ForeignKey(User,default='')
    product_name = models.CharField(max_length=100)
    product_GI = models.CharField(max_length=4)
    product_carb = models.CharField(max_length=4)
    pub_date = models.DateTimeField('date published')

    def __str__(self):              # __unicode__ on Python 2
        return self.product_name

class Note_diary_day(models.Model): #дневная запись
    username = models.ForeignKey(User,default='')
    name = models.CharField(max_length=10)
    pub_date = models.DateTimeField('date published')
    def __str__(self):              # __unicode__ on Python 2
        return self.name

class Note_diary(models.Model): # запись одного приема пищи
    note_diary_date = models.ForeignKey(Note_diary_day,default='')
    name = models.CharField(max_length=10) #название приема пищи
    dose = models.CharField(max_length=4)
    total_carb = models.CharField(max_length=50)
    glu_level = models.CharField(max_length=4)
    pub_date = models.DateTimeField('date published')
    def __str__(self):              # __unicode__ on Python 2
        return self.name

class Note_diary_product(models.Model): #запись одного продукта
    note_diary = models.ForeignKey(Note_diary,default='')
    product_name = models.CharField(max_length=100)
    product_GI = models.CharField(max_length=4)
    product_carb = models.CharField(max_length=4)
    product_mass = models.CharField(max_length=7)
    pub_date = models.DateTimeField('date published')

    def __str__(self):              # __unicode__ on Python 2
        return self.product_name


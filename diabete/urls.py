﻿from django.conf.urls import patterns
from django.conf.urls import url as _url
from mysite import settings
from django.views.generic import RedirectView
from django.contrib.sitemaps.views import sitemap
from django.contrib.sitemaps import Sitemap
from diabete import views
from django.core.urlresolvers import reverse
from datetime import datetime

class StaticViewSitemap(Sitemap):
    def priority(self,obj):
        if obj == 'diabete:index':
            return 1
        else:
            return 0.8

    changefreq = 'daily'

    def lastmod(self, obj):
        return datetime.now()

    def items(self):
        return ['diabete:index', 'diabete:about', 'diabete:contacts']

    def location(self, item):
        return reverse(item)

sitemaps = {
  'static': StaticViewSitemap,
}

urlpatterns = patterns('',
    _url(r'^$', views.IndexView.as_view(), name='index'),
    _url(r'^google8337acc13a0675a4.html/$', views.Google.as_view(), name='google'),
    _url(r'^0c78070968be.html/$', views.Yandex.as_view(), name='yandex'),
    _url(r'^about/$', views.about, name='about'),
    _url(r'^contacts/$', views.contacts, name='contacts'),
    _url(r'^video/$', views.video, name='video'),
    _url(r'^result/$', views.result, name='result'),
    _url(r'^diary/$', views.diary, name='diary'),
    _url(r'^edit_note/$', views.edit_note, name='edit_note'),
    _url(r'^fav_products/$', views.fav_products, name='fav_products'),
    _url(r'^adding_to_fav/$', views.adding_to_fav, name='adding_to_fav'),
    _url(r'^delete_fav/$', views.delete_fav, name='delete_fav'),
    _url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'diabete/login.html','extra_context':{'title':'Войти'}}, name='login'),
    _url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/','extra_context':{'title':'Выйти'}}, name='logout'),
    _url(r'^accounts/register/$', views.register, name='register'),
    (r'^favicon\.ico$',RedirectView.as_view(url=settings.STATIC_URL + 'diabete/favicon.ico')),
    _url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps},name='django.contrib.sitemaps.views.sitemap'),
)

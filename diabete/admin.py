from django.contrib import admin
from diabete.models import Product, Favorite_product
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin

class ProductAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['product_name','product_GI','product_carb']}),
        ('Date information', {'fields': ['pub_date']}),
    ]
    list_display = ('product_name','product_GI','product_carb')
    search_fields = ['product_name']
admin.site.register(Product, ProductAdmin)

class Favorite_productInline(admin.TabularInline):
    model = Favorite_product
    extra = 3
UserAdmin.inlines = [Favorite_productInline]




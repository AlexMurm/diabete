﻿function escapeHtml(text) {
  return text
      .replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");
}

var main = function() {
	var value=parseInt($('#hiddencontent').attr('products_num'));
	var value_fav=value;
  $('.adding').click(function() {
    var post = $('#product option:selected').text();
	var GI = $('#product option:selected').attr('GI');
	var carb = $('#product option:selected').attr('carb');
	var id = $('#product option:selected').attr('id');
	//var value = $('#product option:selected').attr('value');
	if (post) {
    $('#table-chosen-products').find('tbody')
    .append($('<tr>')
        .append($('<td align="center">')
                .text(post)
            ).append($('<td>')
						.append('<input type=text value="'+GI+'" name=product_GI'+id+' maxlength=4 size=4 style="text-align:center">')
				).append($('<td>')
								.append('<input type=text value="'+carb+'" name=product_carb'+id+' maxlength=4 size=4 style="text-align:center">')
					).append($('<td>')
								.append('<input type=text value="0" name=product_mass'+id+' maxlength=4 size=4 style="text-align:center">')
						).append($('<td class="no_border">')
								.append('<button class="btn_del_product" type="button">удалить</button>')
							).append('<input type=hidden value="'+escapeHtml(post)+'" name=product_name'+id+'>')
        )
	}
  });
  $('.adding_to_fav').click(function() {
    var post = $('#product option:selected').text();
	var GI = $('#product option:selected').attr('GI');
	var carb = $('#product option:selected').attr('carb');
	var id = $('#product option:selected').attr('id');
	//var value = $('#product option:selected').attr('value');
	if (post) {
    $('#table-chosen-fav-products').find('tbody')
    .append($('<tr>')
        .append($('<td align="center">')
                .text(post)
            ).append($('<td>')
						.append('<input type=text value="'+GI+'" name=product_GI'+id+' maxlength=4 size=4 style="text-align:center">')
				).append($('<td>')
								.append('<input type=text value="'+carb+'" name=product_carb'+id+' maxlength=4 size=4 style="text-align:center">')
					).append($('<td class="no_border">')
								.append('<button class="btn_del_product" type="button">удалить</button>')
							)
					
        )
	$('#chosen-fav-products').append('<input type=hidden value="'+escapeHtml(post)+'" name=product_name'+id+'>');
	}
  });
  $('.adding-fav').click(function() {
	value = value+1;
    var post = $('#fav-product option:selected').text();
	var GI = $('#fav-product option:selected').attr('GI');
	var carb = $('#fav-product option:selected').attr('carb');
	//var id = $('#fav-product option:selected').attr('id');
	if (post) {
    $('#table-chosen-products').find('tbody')
    .append($('<tr>')
        .append($('<td align="center">')
                .text(post)
            ).append($('<td>')
						.append('<input type=text value="'+GI+'" name=product_GI'+value+' maxlength=4 size=4 style="text-align:center">')
				).append($('<td>')
								.append('<input type=text value="'+carb+'" name=product_carb'+value+' maxlength=4 size=4 style="text-align:center">')
					).append($('<td>')
								.append('<input type=text value="0" name=product_mass'+value+' maxlength=4 size=4 style="text-align:center">')
						).append($('<td class="no_border">')
								.append('<button class="btn_del_product" type="button">удалить</button>')
							)
        )
	
	$('#chosen-products').append('<input type=hidden value="'+escapeHtml(post)+'" name=product_name'+value+'>');
	}
	
  });
  $('.adding_new').click(function() {
	value = value+1;
    $('#table-chosen-products').find('tbody')
    .append($('<tr>')
        .append($('<td align="center">')
                .append('<input type=text value="Ваш продукт" name=product_name'+value+' maxlength=80 size=23 style="text-align:center">')
            ).append($('<td>')
						.append('<input type=text value="0" name=product_GI'+value+' maxlength=4 size=4 style="text-align:center">')
				).append($('<td>')
								.append('<input type=text value=0 name=product_carb'+value+' maxlength=4 size=4 style="text-align:center">')
					).append($('<td>')
								.append('<input type=text value=0 name=product_mass'+value+' maxlength=4 size=4 style="text-align:center">')
						).append($('<td class="no_border">')
								.append('<button class="btn_del_product" type="button">удалить</button>')
							)
        )
	
  });
  $('.adding_new_to_fav').click(function() {
	value_fav = value_fav+1;
    $('#table-chosen-fav-products').find('tbody')
    .append($('<tr>')
        .append($('<td align="center">')
                .append('<input type=text value="Ваш продукт" name=product_name'+value_fav+' maxlength=80 size=23 style="text-align:center">')
            ).append($('<td>')
						.append('<input type=text value=0 name=product_GI'+value_fav+' maxlength=4 size=4 style="text-align:center">')
				).append($('<td>')
								.append('<input type=text value=0 name=product_carb'+value_fav+' maxlength=4 size=4 style="text-align:center">')
					).append($('<td class="no_border">')
								.append('<button class="btn_del_product" type="button">удалить</button>')
							)
        )
	
  });
  $('.edit_fav_products').click(function() {
	var num_users_fav_products=parseInt($('#num_users_fav_products').attr('products_num'));
	for(var  i = 0; i < num_users_fav_products; i++) {
		if($("#check"+i).prop('checked')) {
			value_fav = value_fav+1;
			var product_name = $("#product_name"+i).text();
			var product_GI = $("#product_GI"+i).text();
			var product_carb = $("#product_carb"+i).text();
			$('#table-chosen-fav-products').find('tbody')
				.append($('<tr>')
					.append($('<td align="center">')
						.text(product_name)
						).append($('<td>')
						.append('<input type=text value="'+product_GI+'" name=product_GI'+value_fav+' maxlength=4 size=4 style="text-align:center">')
							).append($('<td>')
								.append('<input type=text value="'+product_carb+'" name=product_carb'+value_fav+' maxlength=4 size=4 style="text-align:center">')
					))
			$('#chosen-fav-products').append('<input type=hidden value="'+escapeHtml(product_name)+'" name=product_name'+value_fav+'>');
		}
	}
	
  });
  $('.select-all').click(function() {
	var state=1; //all is 1
	var num_users_fav_products=parseInt($('#num_users_fav_products').attr('products_num'));
	for(var  i = 0; i < num_users_fav_products; i++) {
		if($( "#check"+i ).prop( "checked" ) == false){
			state = 0;
			break;
		}
	}
	if (state==0) {
		for(var  i = 0; i < num_users_fav_products; i++) {
			$( "#check"+i ).prop( "checked" ,true);
		}
	} else {
		for(var  i = 0; i < num_users_fav_products; i++) {
			$( "#check"+i ).prop( "checked" ,false);
		}
	}
  });
  $('.usual_btn').click(function() {
	var currentInset = $('.active-inset');
    var nextInset = $('.usual-inset');
    var currentSlide = $('.active-slide');
    var nextSlide = $('.usual-slide');
	
    if( !nextSlide.hasClass( "active-slide" ) ) {
		currentInset.removeClass('active-inset');
		nextInset.addClass('active-inset');
		currentSlide.removeClass('active-slide');
		nextSlide.addClass('active-slide');
	}

  });
  $('.favorite_btn').click(function() {
	var currentInset = $('.active-inset');
    var nextInset = $('.fav-inset');
    var currentSlide = $('.active-slide');
    var nextSlide = $('.fav-slide');
	
    if( !nextSlide.hasClass( "active-slide" ) ) {
		currentInset.removeClass('active-inset');
		nextInset.addClass('active-inset');
		currentSlide.removeClass('active-slide');
		nextSlide.addClass('active-slide');
	}

  });
  $('.btn_del_note').click(function() {
	$(this).parent().next().show();
  });
  $('.no_window').click(function() {
	$(this).parent().parent().hide();
  });
  
  $( document ).on( "click", ".btn_del_product", function() {
	$(this).parent().parent().remove();
  }); 
}

$(document).ready(main);

var year_selected = $('#hiddencontent').attr('year');
var month_selected = $('#hiddencontent').attr('month');
var day_selected = $('#hiddencontent').attr('day');

function Calendar2(id, year, month) {
var Dlast = new Date(year,month+1,0).getDate(),
    D = new Date(year,month,Dlast),
    DNlast = new Date(D.getFullYear(),D.getMonth(),Dlast).getDay(),
    DNfirst = new Date(D.getFullYear(),D.getMonth(),1).getDay(),
    calendar = '<tr>',
    month=["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"];
if (DNfirst != 0) {
  for(var  i = 1; i < DNfirst; i++) calendar += '<td>';
}else{
  for(var  i = 0; i < 6; i++) calendar += '<td>';
}
for(var  i = 1; i <= Dlast; i++) {
  if (i == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()) {
    calendar += '<td class="today">' + '<a href="/diary/?date='+D.getFullYear()+'_'+D.getMonth()+'_'+i+'">' + i;
  }else if (i==day_selected && D.getFullYear()==year_selected && D.getMonth()==month_selected){
	calendar += '<td class="selected">' + '<a href="/diary/?date='+D.getFullYear()+'_'+D.getMonth()+'_'+i+'">' + i;
  }else {
    calendar += '<td>' + '<a href="/diary/?date='+D.getFullYear()+'_'+D.getMonth()+'_'+i+'">' + i;
  }
  if (new Date(D.getFullYear(),D.getMonth(),i).getDay() == 0) {
    calendar += '<tr>';
  }
}
for(var  i = DNlast; i < 7; i++) calendar += '<td>&nbsp;';
document.querySelector('#'+id+' tbody').innerHTML = calendar;
document.querySelector('#'+id+' thead td:nth-child(2)').innerHTML = month[D.getMonth()] +' '+ D.getFullYear();
document.querySelector('#'+id+' thead td:nth-child(2)').dataset.month = D.getMonth();
document.querySelector('#'+id+' thead td:nth-child(2)').dataset.year = D.getFullYear();
if (document.querySelectorAll('#'+id+' tbody tr').length < 6) {  // чтобы при перелистывании месяцев не "подпрыгивала" вся страница, добавляется ряд пустых клеток. Итог: всегда 6 строк для цифр
    document.querySelector('#'+id+' tbody').innerHTML += '<tr><td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;';
}
}
Calendar2("calendar2", parseInt(year_selected), parseInt(month_selected));
// переключатель минус месяц
document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(1)').onclick = function() {
  Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month)-1);
}
// переключатель плюс месяц
document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(3)').onclick = function() {
  Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month)+1);
}

